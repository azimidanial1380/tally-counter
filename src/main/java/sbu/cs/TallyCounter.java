package sbu.cs;

public class TallyCounter implements TallyCounterInterface {
    private int tally = 0 ;
    @Override
    public void count() {
        tally++ ;
        if(tally>9999|| tally<0){
            tally = 9999 ;
        }
    }

    @Override
    public int getValue() {
        return tally;
    }

    @Override
    public void setValue(int value) throws IllegalValueException {
        tally = value ;
        if(tally>9999 || tally<0 ){
            throw new IllegalValueException() ;
        }
    }
}
