package sbu.cs;

public class ExerciseLecture4 {

    /*
     *   implement a function that returns factorial of given n
     *   lecture 4 page 15
     */
    public long factorial(int n) {
        if(n == 0){
            return 1 ;
        }
        else{
            if(n>0){
                return (n*factorial(n-1)) ;
            }
        }
        return 0 ;
    }

    /*
     *   implement a function that return nth number of fibonacci series
     *   the series -> 1, 1, 2, 3, 5, 8, ...
     *   lecture 4 page 19
     */
    public long fibonacci(int n) {
        if(n<=2){
            return 1 ;
        }
        else{
            return  ( fibonacci(n-1) + fibonacci( n-2) ) ;
        }
    }

    /*
     *   implement a function that return reverse of a given word
     *   lecture 4 page 19
     */
    public String reverse(String word) {
        StringBuilder str = new StringBuilder(word) ;
        str.reverse() ;
        return str.toString() ;
    }

    /*
     *   implement a function that returns true if the given line is
     *   palindrome and false if it is not palindrome.
     *   palindrome is like 'wow', 'never odd or even', 'Wow'
     *   lecture 4 page 19
     */
    public boolean isPalindrome(String line) {
        String array[] = line.split(" ");
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i  < array.length; i++) {
            sb.append(array[i]);
        }
        String checkString = sb.toString();
        checkString = checkString.toLowerCase() ;
        int j = checkString.length() - 1 ;
        for (int i = 0 ; i < (checkString.length()/2 + 1) ; i++) {
            if( checkString.charAt(i) != checkString.charAt(j) ) {
                return false;
            }
            j-- ;
        }
        return true ;                                      // if it's not false it's true !
    }

    /*
     *   implement a function which computes the dot plot of 2 given
     *   string. dot plot of hello and ali is:
     *       h e l l o
     *   h   *
     *   e     *
     *   l       * *
     *   l       * *
     *   o           *
     *   lecture 4 page 26
     */
    public char[][] dotPlot(String str1, String str2) {
        char[] arr1 ;
        arr1 = new char[str1.length()];
        char[] arr2 ;
        arr2 = new char[str2.length()];
        for(int i = 0 ; i <arr1.length ; i++){
            arr1[i] = str1.charAt(i) ;
        }
        for(int j = 0 ; j <arr2.length ; j++){
            arr2[j] = str2.charAt(j) ;
        }
        char[][] plotOut ;
        plotOut = new char[arr1.length][arr2.length] ;
        for(int i = 0 ; i < arr1.length; i++) {
            for(int j = 0 ; j < arr2.length; j++) {
                if( arr1[i] == arr2[j] ) {
                    plotOut[i][j] = '*' ;
                }
                else{
                    plotOut[i][j] = ' ' ;
                }
            }
        }
        return plotOut;
    }
}
