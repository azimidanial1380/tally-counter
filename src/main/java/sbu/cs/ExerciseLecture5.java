package sbu.cs;

import java.security.SecureRandom;
import java.util.Random;

public class ExerciseLecture5 {

    /*
     *   implement a function to create a random password with
     *   given length using lower case letters
     *   lecture 5 page 14
     */
    public String weakPassword(int length) {

        String strRnd = "";
        for (int i = 0; i < length; i++) {
            Random rnd = new Random();
            char c = (char) ('a' + rnd.nextInt(26));
            strRnd += Character.toString(c);
        }
        return strRnd;
    }
    public String strongPassword(int length) throws Exception {
        if(length < 3)
        {
            throw new
                    IllegalArgumentException("short password") ;
        }
        final String chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ" +
                "abcdefghijklmnopqrstuvwxyz"  ;
        final String numbers  = "0123456789"           ;
        final String specials = "!@#$%^&*()'{}?><"     ;

        SecureRandom random = new SecureRandom();
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < length-2; i++) {
            int randomIndex1 = random.nextInt(chars.length());
            sb.append(chars.charAt(randomIndex1));
        }

        int randomIndex2 = random.nextInt(numbers.length());
        sb.append(numbers.charAt(randomIndex2));

        int randomIndex3 = random.nextInt(specials.length());
        sb.append(specials.charAt(randomIndex3));
        return sb.toString();
    }

    /*
     *   implement a function that checks if a integer is a fibobin number
     *   integer n is fibobin is there exist an i where:
     *       n = fib(i) + bin(fib(i))
     *   where fib(i) is the ith fibonacci number and bin(i) is the number
     *   of ones in binary format
     *   lecture 5 page 17
     */
    public boolean isFiboBin(int n) {
        int x = 1 ;
        int fibobin = 0 ;
        while(fibobin<=n){
            String bin = Integer.toBinaryString(fibonacci(x))  ;
            int binOfFib = 0 ;
            for(int i = 0 ; i < bin.length() ; i++){
                if(bin.charAt(i) == '1') {
                    binOfFib++ ;
                }
            }
            fibobin = fibonacci(x) + binOfFib ;
            if(n == fibobin && n != 3 ) {
                return true ;
            }
            x++ ;
        }
        return false;
    }
    public int fibonacci(int n) {
        if(n==1){
            return n ;
        }
        else if (n<=0) {
            return -1 ;
        }
        else{
            return  ( fibonacci(n-1) + fibonacci( n-2) ) ;
        }
    }
}

