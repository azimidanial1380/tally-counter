package sbu.cs;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ExerciseLecture6 {

    /*
     *   implement a function that takes an array of int and return sum of
     *   elements at even positions
     *   lecture 6 page  16
     */
    public long calculateEvenSum(int[] arr) {
        long sum = 0 ;
        for(int i = 0 ; i < arr.length ; i++){
            if(i%2!=0){
                continue;
            }
            sum += arr[i];
        }
        return sum;
    }

    /*
     *   implement a function that takes an array of int and return that
     *   array in reverse order
     *   lecture 6 page 16
     */
    public int[] reverseArray(int[] arr) {
        int[] reversedArr ;
        reversedArr = new int[arr.length] ;
        int i = 0 ;
        for(int j = arr.length - 1; j >= 0 ; j--){
            reversedArr[j] = arr[i];
            i++ ;
        }
        return reversedArr;
    }

    /*
     *   implement a function that calculate product of two 2-dim matrices
     *   lecture 6 page 21
     */
    public double[][] matrixProduct(double[][] m1, double[][] m2) throws RuntimeException {
        int m1ColLen = m1[0].length;
        int m2RowLen = m2.length;
        if(m1ColLen != m2RowLen) {                  // can't calculate the product ! Illegal matrix dim's !
            throw new IllegalArgumentException() ;
        }
        int mRRowLen = m1.length;
        int mRColLen = m2[0].length;
        double[][] mProduct = new double[mRRowLen][mRColLen];
        for(int i = 0; i < mRRowLen; i++) {
            for(int j = 0; j < mRColLen; j++) {
                for(int k = 0; k < m1ColLen; k++) {
                    mProduct[i][j] += m1[i][k] * m2[k][j];
                }
            }
        }
        return mProduct;
    }

    /*
     *   implement a function that return array list of array list of string
     *   from a 2-dim string array
     *   lecture 6 page 30
     */
    public List<List<String>> arrayToList(String[][] names) {
        List<List<String>> ListOfList = new ArrayList<>() ;
        for (int i = 0 ; i < names.length ; i++){
            List<String> firstDim = new ArrayList<>();
            for(int j = 0 ; j < names[i].length ; j++){
                firstDim.add(names[i][j]) ;
            }
            ListOfList.add(firstDim);
        }
        return  ListOfList ;
    }

    /*
     *   implement a function that return a list of prime factor of integer n
     *   in ascending order
     *   lecture 6 page 30
     */
    public List<Integer> primeFactors(int n) {
        List<Integer> list = new ArrayList<Integer>();
        boolean TF = true ;
        for ( int i = 2 ; i <= n ; i++ ) {
            int pow = 0;
            while ( n % i == 0) {
                n /= i ;
                pow ++ ;
            }
            if ( pow > 0 )	{
                if ( !TF ) {
                }
                list.add(i) ;
                TF = false ;
            }
        }
        return list;
    }

    /*
     *   implement a function that return a list of words in a given string
     *   lecture 6 page 30
     */
    public List<String> extractWord(String line) {

        Pattern p = Pattern.compile("[a-zA-Z]+");

        Matcher m1 = p.matcher(line);
        List<String> list = new ArrayList<String>();
        System.out.println("Words from string \"" + line + "\" : ");
        while (m1.find()) {
            list.add(m1.group());
        }

        return list;
    }
}
